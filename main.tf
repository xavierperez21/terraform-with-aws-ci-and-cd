resource "aws_vpc" "my_vpc" {
  cidr_block = "10.88.0.0/16"

  tags = {
    Name = "luis-vpc-tf-modified"
  }
}

resource "aws_subnet" "my_subnet" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.88.1.0/24"
  availability_zone = "us-west-2a"

  tags = {
    Name = "luis-tf-subnet-modified"
  }
}

resource "aws_network_interface" "nic_luis" {
  subnet_id = aws_subnet.my_subnet.id

  tags = {
    Name = "luis_network_interface-modified"
  }
}

resource "aws_instance" "luis_ec2_tf_awsami" {
  ami           = "ami-0cf6f5c8a62fa5da6" # us-west-2
  instance_type = "t2.micro"

  tags = {
    Name = "luis_ec2_tf_modified"
  }
  
  network_interface {
    network_interface_id = aws_network_interface.nic_luis.id
    device_index         = 0
  }
}